# Express

Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications.

More information you can find at the [expressjs.com](http://expressjs.com/) official website.

# Express Server

Express provides you simple way how to create server for building web applications and APIs. In the following steps you will be introduce how to achieve this.

## Install Express

First things first. If you are in your project directory, you can start with the project initialization. For our purpose we are using npm (You can use Yarn for example or wathever package manager you want or love ❤️).

```cmd
> npm init -y
```

Just for that we are not interested in setting any values in this tutorial, we used the predefined values by using `-y`.

So now we have our `package.json` file and now we can install the express package.

```cmd
> npm install express --save
```

## Create Express Server

After installation is time to write some code. So create our `app.js` file where our super lightweight minimalistic server application will be.

First we need reference the express so first line of the file will be look like this:

```javascript
const express = require('express');
```

Now once we have the express package linked into the our file we can create our application, thats will be handle the HTTP requests (Just like any other server 😄).

```javascript 
const app = express();
```

And we also need tell the application how to handle our HTTP requests.

```javascript
app.get('/', (req, res) => res.send('Hello world!'));
```

It tells to our application:

> JS: Hey app if some HTTP request comes, please send the 'Hello world!' back to the sender as response.
>
> APP: Good to know, thanks my sweet friend...
>
> APP: But wait a minute, where should I have expect the incoming request?

We still have to set the port on which application will be listening to the incoming requests.

```javascript
app.listen(3000, () => console.log('Application listening on port 3000!'));
```

## Run Server

Now we have all work done, so we can try to start your application and send some request to it. Only thing we have to do is tell the Node.js we want to run our application.

```cmd
> node app.js
Application listening on port 3000!
```

In the console now we can see that our application is running and patiently waiting for our requests. Open your web browser on address [localhost:3000](http://localhost:3000/) and now *voilà* (here it is) our response from server tells us 'Hello world!'.

By pressing `Ctrl + C` you can actually stop the application.

# Conclusion

So now our super lightweight and super minimalistic application is done and tested and... *that is the best...* **it works** 🎉!!!

Ok, maybe it is not the most sophisticated thing in the world but now we can create the server with Node.js and we can work on it in the future and we will use it as the starting point of the next tutorual. 

In the next part we will learn something about Express Router and routing in Node.js. 

👋 See you next time.